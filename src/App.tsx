import "./assets/sass/components/App.scss";
import Layout from "./Components/Layout/Layout";
import PaginationContainer from "./Components/PaginationContainer/PaginationContainer";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CombinedSearch from "./Components/CombinedSearch/CombinedSearch";
function App() {
  return (
    <Router>
      <Layout />

      <Switch>
        <Route path="/" exact>
          <PaginationContainer />
        </Route>
        <Route path="/display" exact>
          <CombinedSearch />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
