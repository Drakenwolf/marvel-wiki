/* Characters  comics/stories */

import { ISlice, StorieEntity } from "../../@types/global";
import { MarvelResponse, MARVEL_ENDPOINT } from "./fetchCharacters";

function createEntity(data: { data: { results: any[]; total: any } }) {
  return data.data.results.reduce<ISlice<StorieEntity>>(
    (previous, current) => {
      const newIds = Array.from(previous.ids);
      newIds.push(current.id.toString());

      const newEntites = {
        ...previous.entities,
        [current.id.toString()]: current,
      };

      return { ...previous, entities: newEntites, ids: newIds };
    },
    { ids: [], entities: {}, total: data.data.total }
  );
}

export async function fetchStoriesComics(id: string, page: number) {
  const URL = `${MARVEL_ENDPOINT}stories/${id}/comics?limit=100&offset=${
    5 * page - 1
  }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${
    process.env.REACT_APP_HASH
  }
  `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<StorieEntity>;
  return createEntity(data);
}

export async function fetchStoriesCharacters(id: string, page: number) {
  const URL = `${MARVEL_ENDPOINT}stories/${id}/characters?limit=100&offset=${
    5 * page - 1
  }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${
    process.env.REACT_APP_HASH
  }
  `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<StorieEntity>;
  return createEntity(data);
}

//make a validation for name
