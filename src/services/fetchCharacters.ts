import { BaseEntity, CharacterEntity, ISlice } from "../../@types/global";

export interface MarvelResponse<T = BaseEntity> {
  code: number;
  status: String;
  data: {
    offset: number;
    limit: number;
    total: number;
    count: number;
    results: T[];
  };
}

function createEntity(data: { data: { results: any[]; total: any } }) {
  return data.data.results.reduce<ISlice<CharacterEntity>>(
    (previous, current) => {
      const newIds = Array.from(previous.ids);
      newIds.push(current.id.toString());

      const newEntites = {
        ...previous.entities,
        [current.id.toString()]: current,
      };

      return { ...previous, entities: newEntites, ids: newIds };
    },
    { ids: [], entities: {}, total: data.data.total }
  );
}

export function validateString(string: string) {
  return encodeURIComponent(string.trim());
}

export const MARVEL_ENDPOINT = "https://gateway.marvel.com:443/v1/public/";

export async function fetchCharacters(page: number) {
  const URL = `${MARVEL_ENDPOINT}characters?limit=5&offset=${5 * page - 1
    }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${process.env.REACT_APP_HASH
    }`;
  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse;
  return createEntity(data);
}

/* Filters */

export async function fetchCharacterByName(name: string) {
  const URL = `${MARVEL_ENDPOINT}characters?name=${validateString(
    name
  )}&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${process.env.REACT_APP_HASH
    }
  `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<CharacterEntity>;
  return createEntity(data);
}

/* Characters  comics/stories */

export async function fetchCharactersComics(id: string, page: number) {
  const URL = `${MARVEL_ENDPOINT}characters/${id}/comics?limit=100&offset=${5 * page - 1
    }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${process.env.REACT_APP_HASH
    }
  `;
  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<CharacterEntity>;
  return createEntity(data);
}

export async function fetchCharactersStories(id: string, page: number) {
  const URL = `${MARVEL_ENDPOINT}characters/${id}/stories?limit=100&offset=${5 * page - 1
    }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${process.env.REACT_APP_HASH
    }
  `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<CharacterEntity>;
  return createEntity(data);
}

//make a validation for name
