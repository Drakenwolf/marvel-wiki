import { ComicEntity, ISlice, StorieEntity } from "../../@types/global";
import {
  MarvelResponse,
  MARVEL_ENDPOINT,
  validateString,
} from "./fetchCharacters";

function createEntity(data: { data: { results: any[]; total: any } }) {
  return data.data.results.reduce<ISlice<ComicEntity>>(
    (previous, current) => {
      const newIds = Array.from(previous.ids);
      newIds.push(current.id.toString());

      const newEntites = {
        ...previous.entities,
        [current.id.toString()]: current,
      };

      return { ...previous, entities: newEntites, ids: newIds };
    },
    { ids: [], entities: {}, total: data.data.total }
  );
}

export async function fetchComics(page: number, amount: number) {
  const URL = `${MARVEL_ENDPOINT}comics?limit=${amount}&offset=${
    5 * page - 1
  }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${
    process.env.REACT_APP_HASH
  }`;
  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<ComicEntity>;
  return createEntity(data);
}

/* Filters */

export async function fetchComicByTitle(title: string) {
  const URL = `${MARVEL_ENDPOINT}comics?title=${validateString(
    title
  )}&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${
    process.env.REACT_APP_HASH
  }`;
  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<ComicEntity>;
  return createEntity(data);
}

export async function fetchComicByIssueN(number: number) {
  const URL = `${MARVEL_ENDPOINT}comics?issueNumber=${number}&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${process.env.REACT_APP_HASH}
      `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<ComicEntity>;
  return createEntity(data);
}

export async function fetchComicByFormat(format: string) {
  const URL = `${MARVEL_ENDPOINT}comics?format=${validateString(
    format
  )}&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${
    process.env.REACT_APP_HASH
  }
      `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<ComicEntity>;
  return createEntity(data);
}

/* Characters  comics/stories */

export async function fetchComicsCharacters(id: string, page: number) {
  const URL = `${MARVEL_ENDPOINT}comics/${id}/characters?limit=100&offset=${
    5 * page - 1
  }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${
    process.env.REACT_APP_HASH
  }
  `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<StorieEntity>;
  return createEntity(data);
}

export async function fetchComicsStories(id: string, page: number) {
  const URL = `${MARVEL_ENDPOINT}comics/${id}/stories?limit=100&offset=${
    5 * page - 1
  }&ts=1&apikey=${process.env.REACT_APP_PUBLIC}&hash=${
    process.env.REACT_APP_HASH
  }
  `;

  const res = await fetch(URL);
  const data = (await res.json()) as MarvelResponse<StorieEntity>;
  return createEntity(data);
}

//make a validation for name
