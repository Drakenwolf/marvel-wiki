import { useContext } from "react";
import { typeCombinedSearch } from "../../@types/global";
import DataContext from "../context/DataContext";
import {
  fetchCharacterByName,
  fetchCharacters,
  fetchCharactersComics,
  fetchCharactersStories,
} from "../services/fetchCharacters";
import {
  fetchComicByFormat,
  fetchComicByIssueN,
  fetchComicByTitle,
  fetchComics,
  fetchComicsCharacters,
  fetchComicsStories,
} from "../services/fetchComics";
import {
  fetchStoriesCharacters,
  fetchStoriesComics,
} from "../services/fetchStories";

export default function useMarvelApi() {
  const { state, setState } = useContext(DataContext);

  const setCombinedOption = (combinedOption: typeCombinedSearch) => {
    setState((prevState) => ({
      ...prevState,
      typeCombinedSearch: combinedOption,
    }));
  };

  const setPage = (page: number) => {
    setState((prevState) => ({
      ...prevState,
      page: page,
    }));
  };

  return {
    fetchCharacters: (page: number) => {
      return fetchCharacters(page).then((result) =>
        setState((prevState) => ({
          ...prevState,
          characters: {
            ...result,
            ids: [...new Set(prevState.characters.ids.concat(result.ids))],
            entities: {
              ...prevState.characters.entities,
              ...result.entities,
            },
          },
        }))
      );
    },
    fetchCharacterByName: (name: string) => {
      return fetchCharacterByName(name).then((result) =>
        setState((prevState) => ({
          ...prevState,
          search: {
            ...result,
          },
        }))
      );
    },

    fetchComics: (page: number, amount: number) => {
      return fetchComics(page, amount).then((result) =>
        setState((prevState) => ({
          ...prevState,
          comics: {
            ...result,
            ids: [...new Set(prevState.comics.ids.concat(result.ids))],
            entities: {
              ...prevState.comics.entities,
              ...result.entities,
            },
          },
        }))
      );
    },
    fetchComicByTitle: (title: string) => {
      return fetchComicByTitle(title).then((result) =>
        setState((prevState) => ({
          ...prevState,
          search: {
            ...result,
          },
        }))
      );
    },
    fetchComicByIssueN: (number: number) => {
      return fetchComicByIssueN(number).then((result) =>
        setState((prevState) => ({
          ...prevState,
          search: {
            ...result,
          },
        }))
      );
    },
    fetchComicByFormat: (format: string) => {
      return fetchComicByFormat(format).then((result) =>
        setState((prevState) => ({
          ...prevState,
          search: {
            ...result,
          },
        }))
      );
    },
    fetchCharactersComics: (id: string, page: number) => {
      return fetchCharactersComics(id, page).then((result) => {
        setState((prevState) => ({
          ...prevState,
          combinedSearch: {
            ...result,
          },
        }));
      });
    },
    fetchCharactersStories: (id: string, page: number) => {
      return fetchCharactersStories(id, page).then((result) => {
        setState((prevState) => ({
          ...prevState,
          combinedSearch: {
            ...result,
          },
        }));
      });
    },
    fetchComicsCharacters: (id: string, page: number) => {
      return fetchComicsCharacters(id, page).then((result) => {
        setState((prevState) => ({
          ...prevState,
          combinedSearch: {
            ...result,
          },
        }));
      });
    },
    fetchComicsStories: (id: string, page: number) => {
      return fetchComicsStories(id, page).then((result) => {
        setState((prevState) => ({
          ...prevState,
          combinedSearch: {
            ...result,
          },
        }));
      });
    },

    fetchStoriesComics: (id: string, page: number) => {
      return fetchStoriesComics(id, page).then((result) => {
        setState((prevState) => ({
          ...prevState,
          combinedSearch: {
            ...result,
          },
        }));
      });
    },
    fetchStoriesCharacters: (id: string, page: number) => {
      return fetchStoriesCharacters(id, page).then((result) => {
        setState((prevState) => ({
          ...prevState,
          combinedSearch: {
            ...result,
          },
        }));
      });
    },

    state,
    setState,
    setCombinedOption,
    setPage,
  };
}
