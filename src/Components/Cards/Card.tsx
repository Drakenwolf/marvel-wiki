import React, {
  Dispatch,
  FC,
  SetStateAction,
  useState,
} from "react";
import { Link } from "react-router-dom";
import { ISlice } from "../../../@types/global";
import useMarvelApi from "../../Hooks/useMarvelApi";

const notFound =
  "https://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg";

interface ICard {
  id: string;
  setShowing: Dispatch<SetStateAction<boolean>>;
  showing: boolean;
  SLICE_TYPE: ISlice<any>;
  displayOpt?: boolean;
}

const Card: FC<ICard> = ({
  id,
  showing,
  setShowing,
  SLICE_TYPE,
  displayOpt,
}) => {
  const {
    fetchCharactersStories,
    fetchCharactersComics,
    fetchComicsCharacters,
    fetchComicsStories,
    fetchStoriesCharacters,
    fetchStoriesComics,
    state,
    setCombinedOption,
  } = useMarvelApi();
  const [show, setShow] = useState(false);

  let buttonTop, buttonBottom;
  if (
    state.typeCombinedSearch === "characterStories" ||
    state.typeCombinedSearch === "comicStories"
  ) {
    buttonTop = "Characters";
    buttonBottom = "Comics";
  } else {
    if (displayOpt) {
      buttonTop = "Characters";
      buttonBottom = "Stories";
    } else {
      buttonTop = "Stories";
      buttonBottom = "Comics";
    }
  }
  return (
    <div className={`marvel-card ${show ? "show" : null}`}>
      <div className="marvel-card__image-holder">
        <img
          className="marvel-card__image"
          src={
            SLICE_TYPE.entities[id].thumbnail === null
              ? notFound
              : `${SLICE_TYPE.entities[id].thumbnail.path}.${SLICE_TYPE.entities[id].thumbnail.extension}`
          }
          alt={SLICE_TYPE.entities[id].name || SLICE_TYPE.entities[id].title}
        />
      </div>
      <div className="marvel-card-title">
        <button
          onClick={() => {
            setShowing(!showing);
            setShow(!show);
          }}
          className="toggle-info btn"
        >
          <span className="left"></span>
          <span className="right"></span>
        </button>
        <div className="marvel-card-title_container">
          <h2>
            {SLICE_TYPE.entities[id].name || SLICE_TYPE.entities[id].title}
          </h2>
        </div>
      </div>
      <div className="marvel-card-flap flap1">
        <div className="marvel-card-description">
          {displayOpt ? (
            <>
              <h5>{` issue numebr: ${SLICE_TYPE.entities[id].issueNumber}`}</h5>
              <h6>{`format: ${SLICE_TYPE.entities[id].format}`}</h6>
            </>
          ) : null}

          <p> {SLICE_TYPE.entities[id].description}</p>
        </div>
        <div className="marvel-card-flap flap2">
          <div className="marvel-card-actions d-flex flex-column">
            <Link
              onClick={() => {
                if (
                  state.typeCombinedSearch === "characterStories" ||
                  state.typeCombinedSearch === "comicStories"
                ) {
                  fetchStoriesCharacters(id, state.page);
                  setCombinedOption("storieCharacters");
                } else {
                  if (displayOpt) {
                    fetchComicsCharacters(id, state.page);
                    setCombinedOption("comicCharacters");
                  } else {
                    fetchCharactersStories(id, state.page);
                    setCombinedOption("characterStories");
                  }
                }
              }}
              className="btn mb-2"
              to={"/display"}
            >
              {buttonTop}
            </Link>
            <Link
              onClick={() => {
                if (
                  state.typeCombinedSearch === "characterStories" ||
                  state.typeCombinedSearch === "comicStories"
                ) {
                  fetchStoriesComics(id, state.page);
                  setCombinedOption("storieComics");
                } else {
                  if (displayOpt) {
                    fetchComicsStories(id, state.page);
                    setCombinedOption("comicStories");
                  } else {
                    fetchCharactersComics(id, state.page);
                    setCombinedOption("characterComics");
                  }
                }
              }}
              className="btn"
              to={"/display"}
            >
              {buttonBottom}
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

Card.displayName = "Card";
export default Card;
