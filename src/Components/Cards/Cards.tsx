import  { FC, useState } from "react";
import { ISlice } from "../../../@types/global";
import useMarvelApi from "../../Hooks/useMarvelApi";
import Card from "./Card";
import "../../assets/sass/components/Cards.scss";

interface ICards {
  SLICE_TYPE: ISlice<any>;
  amount: number;
  displayOpt: boolean;
}

const Cards: FC<ICards> = ({ SLICE_TYPE, amount, displayOpt }) => {
  const [showing, setShowing] = useState(false);
  const { state } = useMarvelApi();

  return (
    <div className={`marvel-cards ${showing ? "showing" : null}`}>
      {SLICE_TYPE === state.search && state.search.ids.length === 0 ? (
        <h1>Element not found :(</h1>
      ) : (
        SLICE_TYPE.ids
          .slice(amount * (state.page - 1), amount * (state.page - 1) + amount)
          .map((element) => {
            return (
              <Card
                SLICE_TYPE={SLICE_TYPE}
                key={element}
                id={element}
                showing={showing}
                setShowing={setShowing}
                displayOpt={displayOpt}
              />
            );
          })
      )}
    </div>
  );
};

Cards.displayName = "Cards";
export default Cards;
