/* eslint-disable @typescript-eslint/no-unused-expressions */
import React, {
  Dispatch,
  FC,
  SetStateAction,
  useState,
} from "react";
import useMarvelApi from "../../../Hooks/useMarvelApi";

import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import { searchType } from "../../../../@types/global";

interface ISearch {
  displayOpt: boolean;
  setSearch: Dispatch<SetStateAction<boolean>>;
}

const Search: FC<ISearch> = ({ displayOpt, setSearch }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [searchOpen, setSearchOpen] = useState(false);
  const toggle = () => setDropdownOpen((prevState) => !prevState);
  const toggleSearch = () => setSearchOpen((prevState) => !prevState);
  const [searchOpt, setSearchType] = useState<searchType>("title");

  const {
    fetchCharacterByName,
    fetchComicByTitle,
    fetchComicByIssueN,
    fetchComicByFormat,
  } = useMarvelApi();

  let search = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { search } = e.target as typeof e.target & {
      search: { value: string };
    };
    if (search.value === "") {
      alert("input can not be blank");
    } else {
      switch (searchOpt) {
        case "title":
          displayOpt
            ? fetchComicByTitle(search.value)
            : fetchCharacterByName(search.value);
          break;
        case "issueN":
          fetchComicByIssueN(parseInt(search.value));
          break;
        case "format":
          fetchComicByFormat(search.value);
          break;

        default:
          break;
      }
    }
  };

  return (
    <>
      <div className="container-relative">
        <form
          id="content"
          onSubmit={(e) => {
            search(e);
            setSearch(true);
          }}
        >
          <input
            type="text"
            className={`input ${searchOpen ? "square" : null}`}
            id="search"
          />
          <button
            onClick={() => {
              toggleSearch();
            }}
            type="reset"
            className={`search ${searchOpen ? "close" : null}`}
            id="search-btn"
          ></button>
        </form>
      </div>

      {displayOpt ? (
        <Dropdown isOpen={dropdownOpen} toggle={toggle}>
          <DropdownToggle caret>Filter by</DropdownToggle>
          <DropdownMenu>
            <DropdownItem >
              <button
                onClick={() => {
                  setSearchType("title");
                }}
                className="button draw"
              >
                Title
              </button>
            </DropdownItem>
            <DropdownItem >
              <button
                onClick={() => {
                  setSearchType("issueN");
                }}
                className="button draw"
              >
                Issue Number
              </button>
            </DropdownItem>
            <DropdownItem >
              <button
                onClick={() => {
                  setSearchType("format");
                }}
                className="button draw"
              >
                Format
              </button>
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      ) : (
        <Dropdown isOpen={dropdownOpen} toggle={toggle}>
          <DropdownToggle caret>Filter by</DropdownToggle>
          <DropdownMenu>
            <DropdownItem >
              <button
                onClick={() => {
                  setSearchType("title");
                }}
                className="button draw"
              >
                Name
              </button>
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
      )}
    </>
  );
};

Search.displayName = "Search";
export default Search;
