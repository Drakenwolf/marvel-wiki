/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-expressions */
import { FC, useEffect, useState } from "react";
import {
  Container,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import useMarvelApi from "../../Hooks/useMarvelApi";
import Cards from "../Cards/Cards";
import Search from "./Search/Search";
import "../../assets/sass/components/PaginationContainer.scss";

const PaginationContainer: FC = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen((prevState) => !prevState);
  const [displayOpt, setDisplayOpt] = useState<boolean>(false);
  const [search, setSearch] = useState(false);

  const { fetchCharacters, fetchComics, state, setPage } = useMarvelApi();

  const SLICE_TYPE = search
    ? state.search
    : displayOpt
      ? state.comics
      : state.characters;

  const totalPages = Math.round(
    displayOpt ? state.comics.total : state.characters.total / 5
  );

  useEffect(() => {
    setPage(1);
  }, [displayOpt]);

  useEffect(() => {
    displayOpt ? fetchComics(state.page, 20) : fetchCharacters(state.page);
  }, [state.page, displayOpt]);

  function moreLess(displayOption: boolean) {
    if (displayOption) {
      state.page !== totalPages && setPage(state.page + 1);
    } else {
      state.page !== 1 && setPage(state.page - 1);
    }
  }

  return (
    <>
      <section id="pagination-container" className="container-fluid">
        <div className="m-5">
          <h4>
            Learn of your favorites heros, comics and Stories in Marvels Wiki
          </h4>
          <div className="d-flex justify-content-around align-items-center">
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret>
                {displayOpt ? "Comics" : "Heros"}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem >
                  <button
                    onClick={() => {
                      setDisplayOpt(false);
                      setSearch(false);
                    }}
                    className="button draw"
                  >
                    Heros
                  </button>
                </DropdownItem>
                <DropdownItem>
                  <button
                    onClick={() => {
                      setDisplayOpt(true);
                      setSearch(false);
                    }}
                    className="button draw"
                  >
                    Comics
                  </button>
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>

            <Search displayOpt={displayOpt} setSearch={setSearch} />
          </div>
        </div>

        {/* Grid */}

        <Container>
          <Cards SLICE_TYPE={SLICE_TYPE} amount={5} displayOpt={displayOpt} />
        </Container>

        <div className="d-flex  align-items-center justify-content-center mb-4 mt-4">
          <button
            className="button spin circle"
            onClick={() => {
              moreLess(false);
            }}
          >
            <p className="mb-0">{`<`}</p>
          </button>
          <p className="ms-3 me-3 mb-0">{`page: ${state.page} of ${totalPages}`}</p>
          <button
            className="button spin circle"
            onClick={() => {
              moreLess(true);
            }}
          >
            <p className="mb-0">{`>`}</p>
          </button>
        </div>
      </section>
    </>
  );
};

PaginationContainer.displayName = "PaginationContainer";
export default PaginationContainer;
