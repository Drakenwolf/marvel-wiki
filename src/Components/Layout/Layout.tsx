import { FC, useContext } from "react";

import { MouseContext } from "../../context/MouseContex";

import Cursor from "../Cursor/Cursor";
import Cover from "./Cover/Cover";

const Layout: FC = (props) => {
  const { cursorChangeHandler } = useContext(MouseContext);

  return (
    <>
      <Cursor />
      <div
        onMouseEnter={() => cursorChangeHandler("hovered")}
        onMouseLeave={() => cursorChangeHandler("")}
      >
        <Cover />
        <main id="main">{props.children}</main>
      </div>
    </>
  );
};

Layout.displayName = "Layout";
export default Layout;
