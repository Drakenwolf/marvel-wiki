import  { FC } from "react";
import {  Container } from "reactstrap";
import useMarvelApi from "../../../Hooks/useMarvelApi";
import Cards from "../../Cards/Cards";
import Header from "./Header/Header";
const Cover: FC = (props) => {
  const { state } = useMarvelApi();
  return (
    <>
      <section id="cover" className="d-flex flex-column   container-fluid">
        <Header />
        <h1>
          Space, time, reality. It's more than a linear path. It's a prism of
          <span> endless possibility.</span>
        </h1>

        <Container>
          <Cards SLICE_TYPE={state.characters} amount={1} displayOpt={false} />
        </Container>
      </section>
    </>
  );
};

Cover.displayName = "Cover";
export default Cover;
