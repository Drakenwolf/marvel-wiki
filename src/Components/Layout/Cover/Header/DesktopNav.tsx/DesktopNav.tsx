import { FC } from "react";
import Logo from "../../../../../assets/logo/logo.png";

const DesktopNav: FC = () => {
  return (
    <section id="desktop-nav">
      <div className="d-flex justify-content-between align-items-center">
        <img src={Logo} alt="logo" />
        <ul className="d-flex justify-content-around align-items-center ">
          <li className="nav-menu_container"> Lorem ipmsum</li>

          <li className="nav-menu_container">
            Hover over me
            <ul className="nav-menu">
              <li>
                <a href="/">action 1</a>
              </li>
              <li>
                <a href="/">action 2</a>
              </li>
              <li>
                <a href="/">action 3</a>
              </li>
              <li>
                <a href="/">action 4</a>
              </li>
            </ul>
          </li>

          <li className="nav-menu_container"> Lorem ipmsum</li>

          <li className="nav-menu_container"> Lorem ipmsum</li>
        </ul>
      </div>
    </section>
  );
};

DesktopNav.displayName = "DesktopNav";
export default DesktopNav;
