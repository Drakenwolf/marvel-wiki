import { FC } from "react";
import "../../../../assets/sass/components/Layout.scss";
import MobileNav from "./MovileNav/MobileNav";
import DesktopNav from "./DesktopNav.tsx/DesktopNav";

const Header: FC = () => {
  return (
    <>
      <DesktopNav />
      <MobileNav />
    </>
  );
};

Header.displayName = "Header";
export default Header;
