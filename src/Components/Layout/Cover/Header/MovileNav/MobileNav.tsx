import { FC, useRef } from "react";
import Logo from "../../../../../assets/logo/logo.png";


const MobileNav: FC = () => {
  const hamburgerRef = useRef<HTMLDivElement>(null)
  const menuRef = useRef<HTMLDivElement>(null)

  let open = false;
  const change = () => {
    if (hamburgerRef.current && menuRef.current) {
      if (!open) {
        hamburgerRef.current.classList.add("open");
        menuRef.current.classList.add("menu");
      } else {
        hamburgerRef.current.classList.remove("open");
        menuRef.current.classList.remove("menu");
      }
      open = !open;
    }
  };

  return (
    <section id="mobile-nav">
      <div ref={menuRef} id="overlay">
        <a href="/">Link1</a>
        <a href="/">Link2</a>
        <a href="/">Link3</a>
        <a href="/">Link4</a>
      </div>
      <div className="nav">
        <div className="logo">
          <img src={Logo} alt="" />
        </div>
        <div ref={hamburgerRef} id="hamburger" onClick={() => {
          change();
        }}>
          {/* this empty div add the hamburguer manu with css */}
          <div></div>
        </div>
      </div>
    </section>
  );
};

MobileNav.displayName = "MobileNav";
export default MobileNav;
