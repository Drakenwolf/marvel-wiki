/* eslint-disable @typescript-eslint/no-unused-expressions */
import { FC, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import {
  Container,
} from "reactstrap";
import useMarvelApi from "../../Hooks/useMarvelApi";
import Cards from "../Cards/Cards";
import "../../assets/sass/components/CombinedSearch.scss";

const PaginationContainer: FC = (props) => {
  const { state, setCombinedOption, setPage } = useMarvelApi();
  const totalPages = Math.round(state.combinedSearch.total / 5);

  useEffect(() => {
    console.log(state.combinedSearch)
  }, [state.combinedSearch])

  function moreLess(displayOption: boolean) {
    if (displayOption) {
      state.page !== totalPages && setPage(state.page + 1);
    } else {
      state.page !== 1 && setPage(state.page - 1);
    }
  }

  return (
    <>
      <section id="pagination-container" className="container-fluid">
        <div className="m-5">
          <h4>
            Learn of your favorites heros, comics and Stories in Marvels Wiki
          </h4>

          <h1>
            <Link
              onClick={() => {
                setCombinedOption("");
              }}
              to={"/"}
            >
              Go back to home!
            </Link>
          </h1>
        </div>

        {/* Grid */}

        <Container>
          <Cards
            SLICE_TYPE={state.combinedSearch}
            amount={5}
            displayOpt={false}
          />
        </Container>

        <div className="d-flex  align-items-center justify-content-center mb-4 mt-4">
          <button
            className="button spin circle"
            onClick={() => {
              moreLess(false);
            }}
          >
            <p className="mb-0">{`<`}</p>
          </button>
          <p className="ms-3 me-3 mb-0">{`page: ${state.page} of ${totalPages}`}</p>
          <button
            className="button spin circle"
            onClick={() => {
              moreLess(true);
            }}
          >
            <p className="mb-0">{`>`}</p>
          </button>
        </div>
      </section>
    </>
  );
};

PaginationContainer.displayName = "PaginationContainer";
export default PaginationContainer;

