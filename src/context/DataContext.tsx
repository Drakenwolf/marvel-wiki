import {
  createContext,
  ReactNode,
  useState,
  Dispatch,
  SetStateAction,
} from "react";
import { IContext } from "../../@types/global";

const initialState: IContext = {
  comics: {
    ids: [],
    entities: {},
    total: 0,
  },
  characters: {
    ids: [],
    entities: {},
    total: 0,
  },
  stories: {
    ids: [],
    entities: {},
    total: 0,
  },
  search: {
    ids: [],
    entities: {},
    total: 0,
  },
  combinedSearch: {
    ids: [],
    entities: {},
    total: 0,
  },
  typeCombinedSearch: "",
  page: 1,
};

const DataContext = createContext<{
  state: IContext;
  setState: Dispatch<SetStateAction<IContext>>;
}>({
  state: initialState,
  setState: () => {},
});

export function DataProvider({ children }: { children: ReactNode }) {
  const [data, setData] = useState<IContext>(initialState);

  return (
    <DataContext.Provider value={{ state: data, setState: setData }}>
      {children}
    </DataContext.Provider>
  );
}

export default DataContext;
