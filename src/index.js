import React from "react";
import ReactDOM from "react-dom";
import MouseContextProvider from "./context/MouseContex";
import { DataProvider } from "./context/DataContext";

import "bootstrap/dist/css/bootstrap.min.css";
import App from "./App.tsx";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <React.StrictMode>
    <DataProvider>
      <MouseContextProvider>
        <App />
      </MouseContextProvider>
    </DataProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
