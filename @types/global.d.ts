declare module "global/window" {
  const window: Window;
  export default window;
}
declare module "*.png" {
  const value: any;
  export = value;
}
declare module "*.jpg" {
  const value: any;
  export = value;
}

export type searchType = "title" | "issueN" | "format";

export type typeCombinedSearch =
  | "characterComics"
  | "characterStories"
  | "comicCharacters"
  | "comicStories"
  | "storieCharacters"
  | "storieComics"
  | "";

export interface IContext {
  combinedSearch: ISlice<CombinedSearchEntity>;
  characters: ISlice<CharacterEntity>;
  comics: ISlice<ComicEntity>;
  stories: ISlice<StorieEntity>;
  search: ISlice<T>;
  typeCombinedSearch: typeCombinedSearch;
  page: int;
}

export interface ISlice<T extends BaseEntity> {
  ids: string[];
  entities: {
    [key: string]: T;
  };
  total: int;
}

export interface CombinedSearchEntity extends BaseEntity {}

export interface CharacterEntity extends BaseEntity {}

export interface ComicEntity extends BaseEntity {
  issueNumber: number;
  format: string;
}

export interface StorieEntity extends BaseEntity {}

export interface BaseEntity {
  id: number;
  title?: string;
  name?: string;
  description: string;
  thumbnail: {
    path: string;
    extension: string;
  };
}
